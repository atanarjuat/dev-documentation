# Dev-Documentation

This repository will contain documentation about the LEAP Platform API, examples of valid responses, general strategies that clients should follow when configuring a Provider, selecting gateways and connecting to them. 

## Legacy pointers

Here's a link to the old [bonafide spec](https://0xacab.org/leap/leap_se/-/blob/master/pages/docs/design/bonafide.text).

Many things have changed, it would be good to port and review. Also, desktop & android have diverged a bit from the original spec for historical reasons.

