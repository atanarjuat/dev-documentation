# LEAP Provider API Endpoints

## Reachable with commercially signed CA certifcates:
**Provider JSON**, contains base information for bootstrapping the client. It is recommended to preship the provider.json in the client, contains a fingerprint for the ca.crt: `subdomain.example.org/provider.json`

_Example URL_: `black.riseup.net/provider.json`

_Example Content_: [provider.json](./v3/provider.json)

**Self signed CA certificate** for further API communication: `subdomain.example.org/ca.crt`

_Example URL_: `black.riseup.net/ca.crt`

_Example content_: [ca.crt](./v3/ca.crt)

## Reachable only with the self signed CA certificates:

**Provider JSON**, if the client has a valid CA certificate it's recommended to use this endpoint to update the Provider JSON: `api.subdomain.example.org:<PORT>/provider.json`

_Exmaple URL_: `https://api.black.riseup.net:443/provider.json`

_Example Content_: [provider.json](./v3/provider.json)

**Encrypted Internet Service JSON**, contains information about gateways and their capabilities: `api.subdomain.exmaple.org:<PORT>/<APIVERSION>/config/eip-service.json`

_Example URL_: `https://api.black.riseup.net:443/3/config/eip-service.json`

_Example Content_: [eip-service.json](./v3/eip-service.json)


**Menshen Service**, serves an sorted list the client should try incremantally to connect to a gateway. The sorting depends on the users network location and the load of each gateway (tbd.): `api.subdomain.example.org:<PORT>/json`

_Example URL_: `https://api.black.riseup.net:9001/json`

_Example Content_: [geoservice.json](./v3/geoservice.json)
