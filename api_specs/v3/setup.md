# LEAP Provider API v3

## Deployment diagram that shows which parts are responsible for what, deployment and how the end users interact with them.

Note that this isn't verified against the real world. Some parts can be omitted.

![Deployment diagram](Lilypad API v3-deployment.png)
