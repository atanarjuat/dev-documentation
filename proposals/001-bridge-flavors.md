# Design document 001: Bridge Flavors

* Author: atanarjuat
* Reviewer: cyberta
* Status: under discussion

## Goal

To be able to unequivocally specify different bridge flavors.

## Problem

Up until now in the `eip-service.json` informal spec, the `capabilities.transport` array for each `gateway` object has only contained two values: `openvpn` or `obfs4`. With the landing of `obfsvpn` we're on the verge of starting to deploy different flavors for a single obfuscated transport. It is likely that providers will start deploying a few variants of the same mother transport (example: `kcp-flavor`, `quic-flavor`, `sharknado-flavor`...).

With the upcoming `v4` version of the API we could potentially add new fields to the spec (a proposal for tagging gateways with `experimental` features, for instance, has been proposed in the past). However, we'd like a faster way to deploy bridges that doesn't have to wait until we can coordinate a major api change. If possible, we'd like a transient way to encode bridge flavors (in a way that both servers and clients understand) that is backwards compatible.

On the other hand, we also would like to avoid being super explicit about different variants. Any mapping will for sure be made public, but we think that we need to take incremental steps in the direction of restricting enumeration.

## Proposal

Use the `transport.type` string field to encode flavors according to a well-known table. The authoritative source for translation is this document.

The base name for a transport will be used as a preffix for the non-standard variants. Any variant will append a hyphenated representation of a consecutive integer.

Stable clients will always prefer the standard, non-hyphenated form, that will be considered stable, unless there's an explicit mechanism to opt-in to experimental flavors of a given transport. One such mechanism can be to allow random selection of non-standard bridge transports for beta distribution channels (android/snap). Another might be a cli flag or environment variable, or an explicit setting under "Advanced preferences" that clearly states that user is selecting experimental bridges.

| type            | flavor                    | notes       |
|-----------------|---------------------------|--------------|
| `obfs4`         | obfs4 (yawning)           | still using shapeshifter; will be affected by ntor-handshake backward incomp. bug  |
| `obfs4-hop`     | obfs4+hop (obfsvpn)       | hopping PT, with obfs4 obfuscation |
| `proxy`         | transparent proxy         |              |

Note: `obfs4+kcp` is just done specifying `kcp` as a transport. `obfs4` with `udp` transport is not a legal combination, for obvious reasons.
