# 002: deprecate vpnweb gateway endpoints

* Author: atanarjuat
* Reviewer: your-name-here
* Status: under discussion

## Problem

`vpnweb` exists, historically, because we needed something simple to stop
depending on the legacy ruby codebase (that dealt with registration, databases,
couchdb etc).

However, its current state has some problems:

* Enforcing data structures across clients, and coordinating api changes, is messy.
* We're making enumeration trivial for an adversary.
* We put a lot of complexity on the clients to choose gateways.
* In part, there are some annoyances in the nested json format. The semantics of transport x ports is not obvious.

When `vpnweb` was created, riseup had the need for less gateways. On the other
hand, there were fewer `obfs4` bridges - and only one type of bridge.

For the reasons above, it'd be desirable that:

1. Not all resources are exposed in the `eip-service` array (we want to move towards `private` resource pools).
2. We expose a dynamic endpoint that allows to select a subset of gateways or
   bridges per location - or other properties.

## Proposal

`menshen` was added later, and it consumes `eip-service` as the primary input.
In 2023, we're deploying the load balancing component, so that `menshen` will
have real-time information to skip some nodes from the offered pool (according
to several saturation metrics).

This proposal moves towards having `menshen` as the single entrypoint
coordinator (that in principle consumes the generated `json` structures offered
by `vpnweb` today), and initiate a deprecation process. 

After deprecation is finished, `menshen` will receive the canonical information
directly from any orchestration solution in use - i.e., it will not need to be
configured with the `vpnweb` api endpoint as a needed parameter.

## Consequences

### Downsides:

* The simplicity of serving static files had advantages: there were no runtime
  errors related to any computation. We need to improve testing to catch any possible errors.
* `menshen` would need to inherit other `API` responsibilities now - `VPN` client
  certificate. We might want to consider retaining `vpnweb` as a
  certificate/authentication microservice.

### Advantages:

* We can generate code for consuming the `gateways` API via OpenAPI specs.
