# 003: invite system

* Author: atanarjuat, <add_yourself>
* Reviewers: ...
* Status: draft
* Related: 002-invite-system, 004-introducer

## Problem

By default, `menshen` api is open to the public [^menshen23].
For low-profile deployments, we'd like to add a way to perform resource
allocation, which is able to assign pools of resources to distinct sets of
users. With this ability, menshen can further filter the general pool of
resources by a basic form of ACL.

[menshen23]: by the end of 2023, `menshen` can optionally disable the all-gateways and all-bridges endpoint via a configuration variable.

## Proposal

* Tag all resources (bridges, gateways) with a bucket tag (`bucket1`, `bucket2`...). This tag is needed every time a new resource is added to the pool.
* Encrypt + sign this token, with a random token appended (for revocation): `PAT=S+E(bucket1:user123)`.
* This is a personal access token (`PAT`) that is distributed to users (via a trust anchoring mechanism, off-band).
* Requests to menshen can exchange this PAT by a transient `JWT`, which is in turn used to optionally authenticate as many menshen endpoints as needed (with expiry, renewal, ...).
* Admins can invalidate either buckets or the identifying part of a `PAT`.

...

## Consequences/Discussion

* We might have to revisit the global authentication premise (for the tunnel layer).
* Alternatively, we might want to maintain the global authentication premise (for tunnel) but guard the discovery+access to the bridges, per bucket.

### Downsides:

...

### Advantages:

...
